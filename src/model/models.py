class User():

    def __init__(self, name, email):
        self.name = name
        self.email = email 

    def __repr__(self):
     return self.name

class Project ():
    def __init__(self, name):
        self.name = name

class Sprint ():
    def __init__(self, name, created_data, end_data):
        self.name = name
        self.created_data = created_data
        self.end_data = end_data 

class Task ():
    def __init__(self, name, created_data):
        self.name = name
        self.created_data = created_data