SELECT sdt."name" , p."name"  as "nome_pessoa", p.email , sp.name as "Sprint Name", sp."startDate", sp."endDate" 
FROM scrum_intented_development_task sidt 
inner join scrum_development_task sdt on sdt.id = sidt.id
inner join team_member_scrum_development_task tmsdt on tmsdt.scrum_development_task_id = sidt.id 
inner join team_member tm on tm.id = tmsdt.team_member_id 
inner join person p on p.id  = tm.person_id 
inner join sprint_backlog_scrum_development_task sbsdt on sbsdt.scrum_development_task_id  = sdt.id 
inner join sprint_backlog sb on sb.id = sbsdt.sprint_backlog_id 
inner join sprint sp on sp.id = sb.sprint 
WHERE sidt.id NOT IN (SELECT caused_by FROM scrum_performed_development_task)


CREATE VIEW tarefas_nao_feitas as SELECT sdt."name" , p."name"  as "nome_pessoa", p.email , sp.name as "Sprint Name", sp."startDate", sp."endDate" 
FROM scrum_intented_development_task sidt 
inner join scrum_development_task sdt on sdt.id = sidt.id
inner join team_member_scrum_development_task tmsdt on tmsdt.scrum_development_task_id = sidt.id 
inner join team_member tm on tm.id = tmsdt.team_member_id 
inner join person p on p.id  = tm.person_id 
inner join sprint_backlog_scrum_development_task sbsdt on sbsdt.scrum_development_task_id  = sdt.id 
inner join sprint_backlog sb on sb.id = sbsdt.sprint_backlog_id 
inner join sprint sp on sp.id = sb.sprint 
WHERE sidt.id IN (SELECT caused_by FROM scrum_performed_development_task)


CREATE VIEW tarefas_feitas as SELECT sdt.name , p.name  as "nome_pessoa", p.email , sp.name as "Sprint Name", sp.startDate, sp.endDate 
FROM scrum_intented_development_task sidt 
inner join scrum_development_task sdt on sdt.id = sidt.id
inner join team_member_scrum_development_task tmsdt on tmsdt.scrum_development_task_id = sidt.id 
inner join team_member tm on tm.id = tmsdt.team_member_id 
inner join person p on p.id  = '13'
inner join sprint_backlog_scrum_development_task sbsdt on sbsdt.scrum_development_task_id  = sdt.id 
inner join sprint_backlog sb on sb.id = sbsdt.sprint_backlog_id 
inner join sprint sp on sp.uuid = 'd41666707dc54859b7ded5ca35787d3b' 
WHERE sidt.id IN (SELECT closed_by FROM scrum_performed_development_task);
